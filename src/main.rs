/*
 * Copyright (c) 2020 Lukas Demming
 */
mod editor;
mod ui;

use crate::editor::Editor;

fn main() {
    let mut edit = Editor::new();
    edit.build();
    edit.run();
}
