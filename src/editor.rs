/*
 * Copyright (c) 2020 Lukas Demming
 */
//TODO implement Editor wich uses ropey maybe implemnt own rope at a later point in development
use ropey::{Rope, RopeBuilder};
use std::fs::File;
use std::io;
use std::io::Write;
use std::process::{Command, Stdio};

//Page for the Editor represents one open File
//derive important traits for page
#[derive(Clone, Debug, Default)]
struct Page {
    content: Rope,
}

impl Page {
    pub fn new() -> Self {
        Page {
            content: Rope::new(),
        }
    }
    pub fn new_from_content(content: &str) -> Self {
        Page {
            content: Rope::from_str(content),
        }
    }
}

// Appstate to save the state the editor is in
#[derive(Debug)]
enum AppState {
    Building,
    Saving,
    Running,
    Writing,
    Open,
    Exit,
}
// implementing default for Appstate so its easier to change to default again
impl Default for AppState {
    fn default() -> Self {
        AppState::Writing
    }
}

//Editor struct
#[derive(Debug)]
pub struct Editor {
    working_dir: String,
    pages: Vec<Page>,
    state: AppState,
    open_page: usize,
}

impl Editor {
    pub fn new() -> Self {
        Editor {
            working_dir: Default::default(),
            pages: Vec::new(),
            state: Default::default(),
            open_page: 0,
        }
    }
    pub fn run_program(&mut self) {
        //TODO change dir to working dir and run cargo run + open terminal
        //TODO change state to default after terminating run

        let mut terminated: bool = false;

        while !terminated {
            //TODO run programm
            self.state = Default::default();
            terminated = true;
        }
    }
    pub fn save(&mut self) {
        //TODO save rope to file specified in file_location and working_dir
        //TODO change state to default again after
        let savable = self.pages.get(self.open_page).unwrap().content.clone();

        self.state = Default::default();
        unimplemented!();
    }
    pub fn build(&mut self) {
        //TODO build the app
        //TODO change state to default after building
        let mut build = Command::new("cargo");
        build.current_dir("/home/lukas/Uni/development/rust/Test");

        let command = build.arg("build").output().expect("could not run command");

        //Problem how to get the output of cargo build or cargo run 
        self.state = Default::default();
        //unimplemented!();
    }
    fn open_file(&mut self) {
        //maybe location param
        //TODO make Page instance and add it to pages
        //TODO wenn file empty make new
        //TODO wenn file bereits existiert new from file benutzen
        unimplemented!();
    }
    //loop that runs the application and handles events
    //event = changing the state
    //change to exit state to terminate the application
    pub fn run(&mut self) {
        //TODO Event Loop here
        loop {
            match self.state {
                AppState::Building => (self.build()),
                AppState::Writing => (self.state = AppState::Exit), //just for testing to not create an infinite loop
                AppState::Running => (self.run_program()),
                AppState::Saving => (self.save()),
                AppState::Open => (self.open_file()),
                AppState::Exit => {
                    println!("exiting application");
                    break;
                }
            }
        }
    }
}
